# === This file is part of Charlatan RPSSL ===
#
#   Copyright 2012, Arvind S Raj <sraj.arvind@gmail.com>
#
#   Charlatan RPSSL is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Charlatan RPSSL is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Charlatan RPSSL. If not, see <http://www.gnu.org/licenses/>.
#

import os
import random

from twisted.internet import protocol, reactor
from twisted.protocols.basic import LineReceiver
from twisted.protocols.policies import TimeoutMixin


class CharlatanRPSSLProtocol(LineReceiver, TimeoutMixin):
    def __init__(self):
        self.delimiter = '\r\n'
        self.last_option = ""
        self.score_player = 0
        self.score_shah = 0
        self.timeout_length = 3
        self.setTimeout(self.timeout_length)

    def connectionMade(self):
        self.transport.write(self.factory.greeting_msg + self.delimiter)
        self.transport.write(self.get_score() + self.delimiter)
        self.transport.write(self.select_image() + self.delimiter)
    
    def get_score(self):
        return self.factory.score_msg.format(self.score_shah, self.score_player)

    def get_winner(self):
        if self.score_shah == 15:
            return 'Shah'
        elif self.score_player == 15:
            return 'Player'
        else:
            return None

    def select_image(self):
        option = random.randint(0, len(self.factory.base64.keys()) - 1)
        self.last_option = self.factory.base64.keys()[option]
        return self.factory.base64[self.last_option]

    def lineReceived(self, line):
        response = line.strip()
        possible_answers = self.factory.defeats[self.last_option]
        if response in possible_answers:
            self.transport.write(self.factory.lose_turn_msg + self.delimiter)
            self.score_player = self.score_player + 1
        else:
            self.transport.write(self.factory.win_turn_msg + self.delimiter)
            self.score_shah = self.score_shah + 1

        self.transport.write(self.get_score() + self.delimiter)
        winner = self.get_winner()
        if winner == 'Shah':
            self.transport.write(self.factory.win_game_msg + self.delimiter)
            self.transport.loseConnection()
        elif winner == 'Player':
            response = self.factory.lose_game_msg.format(self.factory.flag)
            self.transport.write(response + self.delimiter)
            self.transport.loseConnection()
        else:
            self.transport.write(self.select_image() + self.delimiter)

    def timeoutConnection(self):
        self.transport.write(self.factory.timeout_msg)
        self.transport.loseConnection()


class CharlatanRPSSLFactory(protocol.Factory):
    protocol = CharlatanRPSSLProtocol
    def __init__(self):
        self.flag = '1$nt_$CTF_fun'
        self.greeting_msg = "Welcome to Charlatan RPSSL"
        self.lose_game_msg = "Oh well you won! Here's your flag: {0}"
        self.lose_turn_msg = "Damn! You won this round!"
        self.score_msg = "Score: Shah {0}-{1} Player"
        self.timeout_msg = "You snooze, you lose!"
        self.win_game_msg = "You've been pwned by the Shah!"
        self.win_turn_msg = "Ha ha! I win!"
        self.initialize()

    def initialize(self):
        self.base64_dir = os.path.join(os.pardir, 'base64')
        self.sha256_dir = os.path.join(os.pardir, 'sha256')
        self.initialize_base64()
        self.initialize_sha256()
        self.initialize_defeat()

    def initialize_base64(self):
        self.base64 = {}
        self.base64['rock'] = file(os.path.join(self.base64_dir, 'rock.b64')).read()
        self.base64['paper'] = file(os.path.join(self.base64_dir, 'paper.b64')).read()
        self.base64['scissors'] = file(os.path.join(self.base64_dir, 'scissors.b64')).read()
        self.base64['spock'] = file(os.path.join(self.base64_dir, 'spock.b64')).read()
        self.base64['lizard'] = file(os.path.join(self.base64_dir, 'lizard.b64')).read()

    def initialize_defeat(self):
        self.defeats = {}
        self.defeats['rock'] = [self.sha256['paper'], self.sha256['spock']]
        self.defeats['paper'] = [self.sha256['scissors'], self.sha256['lizard']]
        self.defeats['scissors'] = [self.sha256['rock'], self.sha256['spock']]
        self.defeats['spock'] = [self.sha256['paper'], self.sha256['lizard']]
        self.defeats['lizard'] = [self.sha256['rock'], self.sha256['scissors']]

    def initialize_sha256(self):
        self.sha256 = {}
        self.sha256['rock'] = file(os.path.join(self.sha256_dir, 'rock.sha256')).read()
        self.sha256['paper'] = file(os.path.join(self.sha256_dir, 'paper.sha256')).read()
        self.sha256['scissors'] = file(os.path.join(self.sha256_dir, 'scissors.sha256')).read()
        self.sha256['spock'] = file(os.path.join(self.sha256_dir, 'spock.sha256')).read()
        self.sha256['lizard'] = file(os.path.join(self.sha256_dir, 'lizard.sha256')).read()


if __name__ == "__main__":
    charlatan_port = 9090
    reactor.listenTCP(charlatan_port, CharlatanRPSSLFactory())
    reactor.run()
